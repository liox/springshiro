package com.shiro;

import com.shiro.config.ShiroConfig;
import com.shiro.realm.CustomerMD5Realm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
class ShiroDemoApplicationTests {

  @Autowired
  ShiroConfig shiroConfig;

  @Test
  void contextLoads() {

    // MD5加密，无随机盐，无散列
    Md5Hash md5Hash01 = new Md5Hash("123456");
    System.out.println(md5Hash01.toHex());

    // MD5+随机盐加密，无散列
    Md5Hash md5Hash02 = new Md5Hash("123456", "1q2w3e");
    System.out.println(md5Hash02.toHex());

    // MD5+随机盐加密+散列1024   配置文件中 "1q2w3e"
    Md5Hash md5Hash03 = new Md5Hash("123456", shiroConfig.getSalt(), 1024);
    System.out.println(md5Hash03.toHex());

  }

  public static void main(String[] args) {
    //// MD5加密，无随机盐，无散列
    //Md5Hash md5Hash01 = new Md5Hash("123456");
    //System.out.println(md5Hash01.toHex());
    //
    //// MD5+随机盐加密，无散列
    //Md5Hash md5Hash02 = new Md5Hash("123456", "1q2w3e");
    //System.out.println(md5Hash02.toHex());
    //
    //// MD5+随机盐加密+散列1024
    //Md5Hash md5Hash03 = new Md5Hash("123456", "1q2w3e", 1024);
    //System.out.println(md5Hash03.toHex());


    testMd5Realm();


  }

  public static void testMd5Realm() {
    // 创建SecurityManager
    DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
    // 设置自定义realm
    CustomerMD5Realm realm = new CustomerMD5Realm();
    // 为realm设置凭证匹配器
    HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
    // 设置加密算法
    credentialsMatcher.setHashAlgorithmName("md5");
    // 设置hash次数
    credentialsMatcher.setHashIterations(1024);
    realm.setCredentialsMatcher(credentialsMatcher);
    defaultSecurityManager.setRealm(realm);
    // 设置安全工具类
    SecurityUtils.setSecurityManager(defaultSecurityManager);
    // 通过安全工具类获取subject
    Subject subject = SecurityUtils.getSubject();
    // 创建token
    UsernamePasswordToken token = new UsernamePasswordToken("christy", "123456");
    try {
      // 登录认证
      subject.login(token);
      System.out.println("认证成功");
    } catch (UnknownAccountException e) {
      e.printStackTrace();
      System.out.println("用户名错误");
    } catch (IncorrectCredentialsException e) {
      e.printStackTrace();
      System.out.println("密码错误");
    }

    //授权
    if (subject.isAuthenticated()) {
      //基于角色权限控制
      System.out.println(subject.hasRole("super"));

      //基于多角色权限控制(同时具有)
      System.out.println(subject.hasAllRoles(Arrays.asList("admin", "super")));

      //是否具有其中一个角色
      boolean[] booleans = subject.hasRoles(Arrays.asList("admin", "super", "user"));
      for (boolean aBoolean : booleans) {
        System.out.println(aBoolean);
      }

      System.out.println("==============================================");

      //基于权限字符串的访问控制  资源标识符:操作:资源类型
      System.out.println("权限:" + subject.isPermitted("user:update:01"));
      System.out.println("权限:" + subject.isPermitted("product:create:02"));

      //分别具有那些权限
      boolean[] permitted = subject.isPermitted("user:*:01", "order:*:10");
      for (boolean b : permitted) {
        System.out.println(b);
      }

      //同时具有哪些权限
      boolean permittedAll = subject.isPermittedAll("user:*:01", "product:create:01");
      System.out.println(permittedAll);
    }
  }

}
