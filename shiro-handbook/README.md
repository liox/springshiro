# SHIRO简单上手手册
- [1.Shiro简介](page/page1.md)
- [2.主要功能及用法](page/page2.md)
- [3.组件demo用法及对应描述](page/page3.md)
- [4.演示截图](page/page4.md)

```
POM使用的相关shiro依赖
	<!-- Shiro 依赖 -->
		<dependency>
			<groupId>org.apache.shiro</groupId>
			<artifactId>shiro-spring</artifactId>
			<version>1.11.0</version>
		</dependency>
		<dependency>
			<groupId>com.github.theborakompanioni</groupId>
			<artifactId>thymeleaf-extras-shiro</artifactId>
			<version>2.0.0</version>
		</dependency>

```


