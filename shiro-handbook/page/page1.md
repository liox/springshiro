## [组件简介，和其他组件的优缺点对比]

### Shiro简介

官网：https://shiro.apache.org/  
Shiro是一个功能强大且易于使用的Java安全框架，它执行身份验证、授权、加密和会话管理。  
使用Shiro易于理解的API，您可以快速轻松地保护任何应用程序—从最小的移动应用程序到最大的web和企业应用程序

### Shiro核心架构

![img.png](../image/img.png)
#### Subject

`Subject`即主体，外部应用与subject进行交互，subject记录了当前的操作用户，将用户的概念理解为当前操作的主体。外部程序通过subject进行认证授权，而subject是通过SecurityManager安全管理器进行认证授权

#### SecurityManager

`SecurityManager`
即安全管理器，对全部的subject进行安全管理，它是shiro的核心，负责对所有的subject进行安全管理。通过SecurityManager可以完成subject的认证、授权等，实质上SecurityManager是通过Authenticator进行认证，通过Authorizer进行授权，通过SessionManager进行会话管理等

`SecurityManager`是一个接口，继承了Authenticator, Authorizer, SessionManager这三个接口

#### Authenticator

`Authenticator`即认证器，对用户身份进行认证，Authenticator是一个接口，shiro提供ModularRealmAuthenticator实现类，通过ModularRealmAuthenticator基本上可以满足大多数需求，也可以自定义认证器

#### Authorizer

`Authorizer`即授权器，用户通过认证器认证通过，在访问功能时需要通过授权器判断用户是否有此功能的操作权限

#### [Realm](https://so.csdn.net/so/search?q=Realm&spm=1001.2101.3001.7020)

`Realm`即领域，相当于datasource数据源，securityManager进行安全认证需要通过Realm获取用户权限数据，比如：如果用户身份数据在数据库那么realm就需要从数据库获取用户身份信息

> 不要把realm理解成只是从数据源取数据，在realm中还有认证授权校验的相关的代码

#### SessionManager

`sessionManager`即会话管理，shiro框架定义了一套会话管理，它不依赖web容器的[session](https://so.csdn.net/so/search?q=session&spm=1001.2101.3001.7020)
，所以shiro可以使用在非web应用上，也可以将分布式应用的会话集中在一点管理，此特性可使它实现单点登录

#### SessionDAO

`SessionDAO`即会话dao，是对session会话操作的一套接口，比如要将session存储到数据库，可以通过jdbc将会话存储到数据库

#### CacheManager

`CacheManager`即缓存管理，将用户权限数据存储在缓存，这样可以提高性能

#### Cryptography

`Cryptography`即密码管理，shiro提供了一套加密/解密的组件，方便开发。比如提供常用的散列、加/解密等功能。

### Shiro中的认证

#### 什么是认证

身份认证，就是判断一个用户是否为合法用户的处理过程。最常用的简单身份认证方式是系统通过核对用户输入的用户名和口令，看其是否与系统中存储的该用户的用户名和口令一致，来判断用户身份是否正确

#### 三个概念

##### Subject

访问系统的用户，主体可以是用户、程序等，进行认证的都称为主体

##### Principal

身份信息，是主体（subject）进行身份认证的标识，标识必须具有`唯一性`，如用户名、手机号、邮箱地址等，一个主体可以有多个身份，但是必须有一个主身份（Primary Principal）

##### credential

凭证信息，是只有主体自己知道的安全信息，如密码、证书等

### Shiro与Spring Security 对比

#### Shiro特点

```
* 内置的基于 POJO 企业会话管理，适用于 Web 以及非 Web 的环境；
* 异构客户端会话访问；
* 非常简单的加密 API；
* 不跟任何的框架或者容器捆绑，可以独立运行。
```

#### Spring Security特点

```
* 不能脱离Spring；
* spring-security对spring结合较好，项目是spring-boot等搭建的，使用起来更加方便；
* 有更好的spring社区进行支持；
* 支持oauth授权。
```

#### 相同点

```
* 认证功能
* 授权功能
* 加密功能
* 会话管理
* 缓存支持
```

#### 不同点

```
* Spring Security是一个重量级的安全管理框架；Shiro则是一个轻量级的安全管理框架
* Spring Security 基于Spring开发，项目若使用Spring作为基础，配合Spring Security 做权限更便捷，而Shiro需要和Spring 进行整合开发；
* Spring Security 功能比Shiro更加丰富些，例如安全维护方面；
* Spring Security 社区资源相对于Shiro更加丰富；
* Shiro 的配置和使用比较简单，Spring Security 上手复杂些；
* Shiro 依赖性低，不需要任何框架和容器，可以独立运行， Spring Security依赖Spring容器；
* Shiro 不仅仅可以使用在web中，它可以工作在任何应用环境中。在集群会话时Shiro最重要的一个好处或许就是它的会话是独立于容器的；
```
