## [组件demo用法及对应描述]

### Controller 中login方法
```
@RequestMapping("/login")
  public String login(String username, String password, Model model) {
    //创建一个shiro的Subject对象，利用这个对象来完成用户的登录认证
    Subject subject = SecurityUtils.getSubject();

    //判断当前用户是否已经认证过，如果已经认证过着不需要认证;如果没有认证过则完成认证
    if (!subject.isAuthenticated()) {
      //创建一个用户账号和密码的Token对象，并设置用户输入的账号和密码
      //这个对象将在Shiro中被获取
      UsernamePasswordToken token = new UsernamePasswordToken(username, password);
      try {
        //如账号不存在或密码错误等等，需要根据不同的异常类型来判断用户的登录状态，并给予友好的信息提示
        //调用login后，Shiro就会自动执行自定义的Realm中的认证方法
        subject.login(token);

      } catch (UnknownAccountException e) {
        //表示用户的账号错误，这个异常是在后台抛出
        log.info("---------------账号不存在");
        model.addAttribute("errorMessage", "账号不存在");
        return "login";
      } catch (LockedAccountException e) {
        //表示用户的账号被锁定，这个异常是在后台抛出
        log.info("===============账号被锁定");
        model.addAttribute("errorMessage", "账号被冻结");
        return "login";
      } catch (IncorrectCredentialsException e) {
        //表示用户的密码错误，这个异常是shiro在认证密码时抛出
        log.info("***************密码不匹配");
        model.addAttribute("errorMessage", "密码错误");
        return "login";
      }
    }
    return "redirect:/success";
  }```

### MD5测试类
```  public static void testMd5Realm() {
    // 创建SecurityManager
    DefaultSecurityManager defaultSecurityManager = new DefaultSecurityManager();
    // 设置自定义realm
    CustomerMD5Realm realm = new CustomerMD5Realm();
    // 为realm设置凭证匹配器
    HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
    // 设置加密算法
    credentialsMatcher.setHashAlgorithmName("md5");
    // 设置hash次数
    credentialsMatcher.setHashIterations(1024);
    realm.setCredentialsMatcher(credentialsMatcher);
    defaultSecurityManager.setRealm(realm);
    // 设置安全工具类
    SecurityUtils.setSecurityManager(defaultSecurityManager);
    // 通过安全工具类获取subject
    Subject subject = SecurityUtils.getSubject();
    // 创建token
    UsernamePasswordToken token = new UsernamePasswordToken("christy", "123456");
    try {
      // 登录认证
      subject.login(token);
      System.out.println("认证成功");
    } catch (UnknownAccountException e) {
      e.printStackTrace();
      System.out.println("用户名错误");
    } catch (IncorrectCredentialsException e) {
      e.printStackTrace();
      System.out.println("密码错误");
    }

    //授权
    if (subject.isAuthenticated()) {
      //基于角色权限控制
      System.out.println(subject.hasRole("super"));

      //基于多角色权限控制(同时具有)
      System.out.println(subject.hasAllRoles(Arrays.asList("admin", "super")));

      //是否具有其中一个角色
      boolean[] booleans = subject.hasRoles(Arrays.asList("admin", "super", "user"));
      for (boolean aBoolean : booleans) {
        System.out.println(aBoolean);
      }

      System.out.println("==============================================");

      //基于权限字符串的访问控制  资源标识符:操作:资源类型
      System.out.println("权限:" + subject.isPermitted("user:update:01"));
      System.out.println("权限:" + subject.isPermitted("product:create:02"));

      //分别具有那些权限
      boolean[] permitted = subject.isPermitted("user:*:01", "order:*:10");
      for (boolean b : permitted) {
        System.out.println(b);
      }

      //同时具有哪些权限
      boolean permittedAll = subject.isPermittedAll("user:*:01", "product:create:01");
      System.out.println(permittedAll);
    }
  }
  ```
