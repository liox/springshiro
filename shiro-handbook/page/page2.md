## [主要功能及用法]

### ShiroConfig 配置类

- 1.配置SecurityManager
- 2.配置自定义Realm(Realm是顶级接口，可以继承次级抽象类，然后覆盖getName，supports，getAuthenticationInfo方法)
- 3.配置自定义ShiroFilterFactoryBean,为过滤器创建规则

```
/**
 * 当前类是一个Spring的配置类，用于模拟Spring的配置文件
 *
 * 1、Subject：
 * 即“当前操作用户”。它仅仅意味着“当前跟软件交互的东西”。
 * Subject代表了当前用户的安全操作，SecurityManager则管理所有用户的安全操作。
 *
 * 2、SecurityManager：
 * 它是Shiro框架的核心，典型的Facade模式，Shiro通过SecurityManager来管理内部组件实例，并通过它来提供安全管理的各种服务。
 *
 * 3、Realm：
 * Realm充当了Shiro与应用安全数据间的“桥梁”或者“连接器”。当对用户执行认证（登录）和授权（访问控制）验证时，Shiro会从应用配置的Realm中查找用户及其权限信息。
 *
 * @Configuration： 表示当前类作为配置文件使用（配置容器，放在类的上面；等同 xml 配置文件，可以在其中声明 bean ）
 * @Bean： 将对象注入到Spring容器中（类似<bean>标签，放在方法上面）；不指定对象的名称，默认是方法名是 id
 */
@Configuration
@PropertySource(value = "classpath:shiro.properties")
public class ShiroConfig {
  /** salt为测试用的值，正式程序续从数据库取，没有该项目 */
  @Value("salt")
  private String salt;

  public String getSalt() {
    return salt;
  }
  

  /**
   * 配置Shiro的安全管理器
   *
   * @Bean： 将对象注入到Spring容器中（类似<bean>标签，放在方法上面）；不指定对象的名称，默认是方法名是 id
   */
  @Bean
  public SecurityManager securityManager(Realm myRealm) {
    DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
    //设置一个Realm，这个Realm是最终用于完成我们的认证号和授权操作的具体对象
    securityManager.setRealm(myRealm);
    return securityManager;
  }

  /**
   * 配置一个自定义的Realm的bean，最终将使用这个bean返回的对象来完成我们的认证和授权
   */
  @Bean
  public MyRealm myRealm() {
    MyRealm myRealm = new MyRealm();
    return myRealm;
  }

  /**
   * 配置一个Shiro的过滤器bean，这个bean将配置Shiro相关的一个规则的拦截
   * 如什么样的请求可以访问，什么样的请求不可以访问等等
   */
  @Bean
  public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
    //创建Shiro的拦截的拦截器 ，用于拦截我们的用户请求
    ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();

    //设置Shiro的安全管理，设置管理的同时也会指定某个Realm 用来完成我们权限分配
    shiroFilter.setSecurityManager(securityManager);
    //用于设置一个登录的请求地址，这个地址可以是一个html或jsp的访问路径，也可以是一个控制器的路径
    //作用是用于通知Shiro我们可以使用这里路径转向到登录页面，但Shiro判断到我们当前的用户没有登录时就会自动转换到这个路径
    //要求用户完成成功
    shiroFilter.setLoginUrl("/");
    //登录成功后转向页面，由于用户的登录后期需要交给Shiro完成，因此就需要通知Shiro登录成功之后返回到那个位置
    shiroFilter.setSuccessUrl("/success");
    //用于指定没有权限的页面，当用户访问某个功能是如果Shiro判断这个用户没有对应的操作权限，那么Shiro就会将请求
    //转向到这个位置，用于提示用户没有操作权限
    shiroFilter.setUnauthorizedUrl("/noPermission");

    //定义一个Map集合，这个Map集合中存放的数据全部都是规则，用于设置通知Shiro什么样的请求可以访问,什么样的请求不可以访问
    Map<String, String> filterChainMap = new LinkedHashMap<String, String>();

    // /login 表示某个请求的名字；anon 表示可以使用游客进行登录（这个请求不需要登录）
    filterChainMap.put("/login", "anon");

    //我们可以在这里配置所有的权限规则，这列数据需要从数据库中读取出来

    //或者在控制器中添加Shiro的注解
    /**
     /admin/**  表示一个请求名字的通配， 以admin开头的任意子路径下的所有请求
     authc 表示这个请求需要进行认证（登录），只有认证（登录）通过才能访问
     注：
     ** 表示任意子路径
     *  表示任意的一个路径
     ? 表示 任意的一个字符
     */
    filterChainMap.put("/admin/**", "authc");
    filterChainMap.put("/user/**", "authc");

    //表示所有的请求路径全部都需要被拦截登录，这个必须必须写在Map集合的最后面,这个选项是可选的
    //如果没有指定/** 那么如果某个请求不符合上面的拦截规则Shiro将方行这个请求
    filterChainMap.put("/**", "authc");

    shiroFilter.setFilterChainDefinitionMap(filterChainMap);

    // liox:
    // 为了避开
    // No SecurityManager accessible to the calling code, either bound to the org.apache.shiro.util.
    // ThreadContext or as a vm static singleton.
    // This is an invalid application configuration.
    // 打的补丁
    SecurityUtils.setSecurityManager(securityManager);

    return shiroFilter;
  }
}

```

### 自定义Realm
Realm是顶级接口，可以继承次级抽象类，然后覆盖getName，supports，getAuthenticationInfo方法

- AuthenticatingRealm Realm接口的顶级抽象实现，仅实现身份验证支持（登录）操作，并将授权（访问控制）行为留给子类。
- AuthorizingRealm 通过添加授权（访问控制）支持扩展了AuthenticatingRealm的功能。
- CachingRealm Realm 接口的一个非常基本的抽象扩展点，为子类提供缓存支持
- SimpleAccountRealm Realm接口的简单实现，它使用一组配置的用户帐户和角色来支持身份验证和授权。每个帐户条目指定用户的用户名、密码和角色。角色也可以映射到权限并与用户相关联。


```
/**
 * 自定义Realm，用来实现用户的认证和授权
 * Realm：父类抽象类
 */
public class MyRealm implements Realm {

  @Autowired
  ShiroConfig shiroConfig;

  /**
   * 返回分配给此Realm的（应用程序唯一的）名称。为单个应用程序配置的所有领域都必须具有唯一的名称。
   * 返回值：
   * 分配给此Realm的（应用程序唯一的）名称。
   */
  @Override
  public String getName() {
    return "shirodemo";
  }

  /**
   * 用来验证token类型
   *
   * @param authenticationToken the AuthenticationToken submitted for the authentication attempt
   * @return
   */
  @Override
  public boolean supports(AuthenticationToken authenticationToken) {
    // 此处可以判断token类型是否支持，测试默认固定返回true
    return true;
  }

  @Override
  public AuthenticationInfo getAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
    //将AuthenticationToken强转成UsernamePasswordToken 这样获取账号和密码更加的方便
    UsernamePasswordToken token = (UsernamePasswordToken)authenticationToken;

    //获取用户在浏览器中输入的账号
    String userName = token.getUsername();

    //  liox:20230129  此处实现业务逻辑
    //认证账号,正常情况我们需要这里从数据库中获取账号的信息，以及其他关键数据，例如账号是否被冻结等等
    String dbUserName = userName;

    if (!"admin".equals(dbUserName) && !"zhangsan".equals(dbUserName)) {//判断用户账号是否正确
      throw new UnknownAccountException("账号错误");
    }
    if ("zhangsan".equals(userName)) {
      throw new LockedAccountException("账号被锁定");
    }
    //定义一个密码(这个密码应该来自数据库)
    String dbpassword = "a18d2133f593d7b0e3ed488560404083";

    //获取用户在浏览器中输入的密码
    String userPass = String.valueOf(token.getPassword());

    // MD5+随机盐加密+散列1024   配置文件中 "1q2w3e"，实际应该自动生成并存库
    Md5Hash md5Hash03 = new Md5Hash(userPass, shiroConfig.getSalt(), 1024);

    if (!dbpassword.equals(md5Hash03.toHex())) {
      throw new IncorrectCredentialsException("密码错误");
    }

    /**
     * 创建密码认证对象，由Shiro自动认证密码
     * 参数1  数据库中的账号（页面账号也可）
     * 参数2  数据库中的密码
     * 参数3  当前Relam的名字
     * 如果密码认证成功，会返回一个用户身份对象；如果密码验证失败则抛出异常
     */
    //认证密码是否正确
    return new SimpleAuthenticationInfo(dbUserName, dbpassword, getName());
  }
}
```
